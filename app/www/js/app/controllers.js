/**
 * beginnings of a controller to login to system
 * here for the purpose of showing how a service might
 * be used in an application
 */
angular.module('app.controllers', [])
    .controller('ListDetailCtrl', [
        '$state', '$scope', '$stateParams', 'UserService',   // <-- controller dependencies
        function ($state, $scope, $stateParams, UserService) {

            $scope.index = $stateParams.itemId;

        }])
    .controller('DashCtrl', [
        '$state', '$scope', 'UserService', 'SavedWordListService', // <-- controller dependencies
        function ($state, $scope, UserService, SavedWordListService) {
            $scope.collections = [];
            $scope.canShowCollections = false;
            var refresh = function() {
                SavedWordListService.getAll().then(function(wordLists){
                    $scope.collections = wordLists;
                });
                if ($scope.collections.length > 0) {
                    $scope.canShowCollections = true;
                } else {
                    $scope.canShowCollections = false;
                }
            }

            $scope.$on('$ionicView.enter', function() {
                // Code you want executed every time view is opened
                refresh();
                console.log("View refreshed!")
            });

        }])
.controller('GroupListCtrl', [
        '$state', '$scope', '$ionicModal', '$ionicPopup', 'UserService', 'WordGroupService',  'WordService', // <-- controller dependencies
        function ($state, $scope, $ionicModal, $ionicPopup, UserService, WordGroupService, WordService) {
            $scope.groups = [];
            $scope.canShowGroups = false;
            var refresh = function() {
                WordGroupService.getAll().then(function(wordGroups){
                    if (wordGroups.length > 0) {
                        $scope.canShowGroups = true;
                    } else {
                        $scope.canShowGroups = false;
                    }
                    $scope.groups = wordGroups;
                    for (var i = 0; i < $scope.groups.length ; i++) {
                        $scope.groups[i] = {
                            object: $scope.groups[i],
                            id: $scope.groups[i].id,
                            name: $scope.groups[i].get("name"),
                            description: $scope.groups[i].get("description")
                        }
                    };
                });
                console.log("View refreshed!");
            }

            $scope.$on('$ionicView.enter', function() {
                // Code you want executed every time view is opened
                refresh();
            });

            // Create and load the "Add" Modal
            $scope.group = {
                name: "",
                description: ""
            }
            $ionicModal.fromTemplateUrl('templates/group-add-modal.html', function(modal) {
              $scope.addGroupModal = modal;
            }, {
              scope: $scope,
              animation: 'slide-in-up',
              focusFirstInput: true,
              backdropClickToClose: true,
              hardwareBackButtonClose: true
            });

              // Called when the form is submitted
              $scope.confirmAddGroup = function(group) {
                $scope.validationError = false;
                $scope.errorMsg = "";
                if (group.name == null || group.name == "") {
                    $scope.validationError = true;
                    $scope.errorMsg = "Please enter a name for the group.";
                    return;
                }
                WordGroupService.add(group.name, group.description);
                $scope.addGroupModal.hide();
                group.name = "";
                group.description = "";
                refresh();
              };

              // Open our new task modal
              $scope.addGroup = function() {
                $scope.addGroupModal.show();
              };

              // Close the new task modal
              $scope.closeAddGroupModal = function() {
                $scope.addGroupModal.hide();
              };

              // Process a remove request
            $scope.confirmRemove = function(group) {
                // A confirm dialog
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove Group',
                    template: 'Are you sure that you want to <strong>remove</strong> this group?',
                    okText: 'Yes, remove', // String (default: 'OK'). The text of the OK button.
                    okType: 'button-assertive', // String (default: 'button-positive'). The type of the OK button.
                });
                confirmPopup.then(function(response) {
                    if (response) {
                        WordGroupService.delete(group.object);
                        refresh();
                    }
                });
            }

        }])
.controller('GroupDetailCtrl', [
        '$state', '$stateParams', '$scope', '$ionicModal', '$ionicPopup', 'UserService', 'WordGroupService',  'WordService', // <-- controller dependencies
        function ($state, $stateParams, $scope, $ionicModal, $ionicPopup, UserService, WordGroupService, WordService) {
            $scope.words = [];
            $scope.group = {}
            $scope.canShowWords = false;
            var refresh = function() {
                WordGroupService.getById($stateParams.groupId).then(function(group) {
                    $scope.group = {
                        object: group,
                        id: group.id,
                        name: group.get("name"),
                        description: group.get("description")
                  }
                }).then(function(){
                    return WordService.getAllFromGroup($scope.group.object);
                }).then(function(words){
                    if (words.length > 0) {
                        $scope.canShowWords = true;
                    } else {
                        $scope.canShowWords = false;
                    }
                    $scope.words = words;
                    for (var i = 0; i < $scope.words.length ; i++) {
                        $scope.words[i] = {
                            object: $scope.words[i],
                            id: $scope.words[i].id,
                            value: $scope.words[i].get("value")
                        }
                    };
                });
                console.log("View refreshed!");
            }

            $scope.$on('$ionicView.enter', function() {
                // Code you want executed every time view is opened
                refresh();
            });

            // Create and load the "Add" Modal
            $scope.word = {
                word: "",
                group: $scope.group
            }
            $ionicModal.fromTemplateUrl('templates/word-add-modal.html', function(modal) {
              $scope.addWordModal = modal;
            }, {
              scope: $scope,
              animation: 'slide-in-up',
              focusFirstInput: true,
              backdropClickToClose: true,
              hardwareBackButtonClose: true
            });

              // Called when the form is submitted
              $scope.confirmAddWord = function(word) {
                $scope.validationError = false;
                $scope.errorMsg = "";
                if (word.value == null || word.value == "") {
                    $scope.validationError = true;
                    $scope.errorMsg = "Please enter the word you want to add.";
                    return;
                }
                WordService.add(word.value, $scope.group.object);
                $scope.addWordModal.hide();
                word.value = "";
                refresh();
              };

              // Open our new task modal
              $scope.addWord = function() {
                $scope.addWordModal.show();
              };

              // Close the new task modal
              $scope.closeAddWordModal = function() {
                $scope.addWordModal.hide();
              };

              // Process a remove request
            $scope.confirmRemove = function(word) {
                // A confirm dialog
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove Word',
                    template: '<strong>Remove</strong> this word from the group?',
                    okText: 'Yes, remove', // String (default: 'OK'). The text of the OK button.
                    okType: 'button-assertive', // String (default: 'button-positive'). The type of the OK button.
                });
                confirmPopup.then(function(response) {
                    if (response) {
                        WordService.delete(word.object);
                        refresh();
                    }
                });
            }

        }])
        .controller('BrainstormCtrl', [
        '$state', '$scope', '$filter', 'WordGroupService', 'WordService', 'SavedWordListService', 'UserService',   // <-- controller dependencies
        function ($state, $scope, $filter, WordGroupService, WordService, SavedWordListService, UserService) {

            $scope.groups = [];
            $scope.words = [];
            $scope.selectedGroups = [];
            $scope.enoughGroups = false;
            $scope.enoughWords = true;
            $scope.collection = [];
            var refresh = function() {
                WordGroupService.getAll().then(function(wordGroups){
                    if (wordGroups.length > 1) {
                        $scope.enoughGroups = true;
                    } else {
                        $scope.enoughGroups = false;
                    }
                    $scope.groups = wordGroups;
                    for (var i = 0; i < $scope.groups.length ; i++) {
                        $scope.groups[i] = {
                            object: $scope.groups[i],
                            id: $scope.groups[i].id,
                            name: $scope.groups[i].get("name"),
                            description: $scope.groups[i].get("description"),
                            selected: true
                        }
                    };
                }).then(function(){
                    return WordService.getAll().then(function(words){
                        console.log("words", words);
                        return words;
                    });
                }).then(function(words){
                    if (words.length > 1) {
                        $scope.enoughWords = true;
                    } else {
                        $scope.enoughWords = false;
                    }
                });
                console.log("View refreshed!");
            }

            getSelected = function(groups) {
                $scope.selectedGroups = $filter('filter')(groups, {selected: true});
            }

            $scope.generateCollection = function(groups) {
                $state.go('tab.brainstorm-result');
                /*getSelected(groups);
                var result = [];
                 
                for (var i = 0; i < $scope.selectedGroups.length; i++) {
                    window.localStorage["mykey"] = "";
                    result[i] = WordService.getRandomFromGroup($scope.selectedGroups[i].object);
                    wordObj  = result[i];
                    wordObj.then(function(prom){
                        window.localStorage["mykey"]=prom[0].get("value");
                        // console.log( window.localStorage["mykey"])
                    });
                    setTimeout(function(){
                         console.log(1);
                          console.log(1);
                           console.log(1);
                            console.log(1);
                    },4000)
                    $scope.collection.push({
                            group: $scope.selectedGroups[i].name,
                            value:window.localStorage["mykey"]
                   })
                    console.log("foo")   
                    console.log(window.localStorage["mykey"])
                }*/
                // $scope.collection = window.localStorage["mykey"];
            }

            $scope.$on('$ionicView.enter', function() {
                // Code you want executed every time view is opened
                refresh();
            });

        }])
        .controller('WordDetailCtrl', [
        '$state', '$scope', 'WordGroupService', 'WordService', 'SavedWordListService', 'UserService',   // <-- controller dependencies
        function ($state, $scope, checkboxFilter, WordGroupService, WordService, SavedWordListService, UserService) {

        }])
    .controller('SettingsCtrl', [
        '$state', '$scope', 'UserService',   // <-- controller dependencies
        function ($state, $scope, UserService) {

            UserService.currentUser().then(function (_user) {
                $scope.user = {
                    firstName: _user.get("first_name"),
                    lastName: _user.get("last_name"),
                    email: _user.get("email")
                }
            });

            $scope.doLogoutAction = function () {
                UserService.logout().then(function () {

                    // transition to next state
                    $state.go('app-login');

                }, function (_error) {
                    alert("Error logging in: " + _error.debug);
                })
            };


        }]);

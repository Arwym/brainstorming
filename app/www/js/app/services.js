angular.module('app.services', [])
    .service('WordService', ['$q', 'UserService', function($q, UserService){
        // Words: add, edit, delete, getAllFromGroupId, getRandomFromGroupId, getWordById
        var parseObject = Parse.Object.extend("Word");
        return  {
            object: parseObject,
            /**
             * Adds a word.
             * @param {String} word - word to add
             * @param {Object} group - object representing the group to add the word to
             * @param {Object} user - user or pointer to user to associate word to. (optional)
             *                        by default word is associated to current user.
             * @return {Promise} resolved when the object is saved, rejected on any error.
             */
            add : function(word, group, user){
                var d = $q.defer();
                if(user){
                    d.resolve(user);
                } else {
                    UserService.currentUser().then(d.resolve.bind(d));
                }
                return d.promise.then(function(user){
                    var wordObject = new parseObject();
                    wordObject.set('group', group);
                    wordObject.set('timesSaved', 0);
                    wordObject.set('timesUsed', 0);
                    wordObject.set('user', user);
                    wordObject.set('value', word);
                    return wordObject.save();
                });
            },
            /**
             * Edits a word.
             * @param {String} wordObject - object representing the word.
             * @param {Object} word - new word
             * @param {Object} group - new group
             * @return {Promise} resolved when the object is edited, rejected on any error.
             */
            edit : function(wordObject, word, group){
                wordObject.set("word", word);
                wordObject.set("group", group);
                return wordObject.save();
            },
            /**
             * Deletes a word.
             * @param {String} wordObject - object representing the word.
             * @return {Promise} resolved when the object is deleted, rejected on any error.
             */
            delete : function(wordObject){
                return wordObject.destroy();
            },
            /**
             * Gets all words belonging to a group.
             * @param {Object} wordGroup - object representing the group.
             * @return {Promise} resolved with all words in the group, rejected on any error.
             */
            getAllFromGroup : function(wordGroup){
                var query = new Parse.Query(parseObject);
                query.equalTo('group', wordGroup);
                return query.find();
            },
            /**
             * Gets all words.
             * @return {Promise} resolved with all words in the group, rejected on any error.
             */
            getAll : function(user){
                var query = new Parse.Query(parseObject);
                var promise;
                if(user){
                    promise = $q.when(user);
                } else {
                    promise = UserService.currentUser();
                }
                return $q.when(promise.then(function(user){
                    query.equalTo('user', user);
                }).then(function(){
                    return query.find();
                }));
            },
            /**
             * Gets one random word belonging to a given group.
             * @param {Object} wordGroup - object representing the group.
             * @return {Promise} resolved with one random word from the group, rejected on any error or if there are no words in the group.
             */
            getRandomFromGroup : function(wordGroup){
                var query = new Parse.Query(parseObject);
                query.equalTo('group', wordGroup);
                return query.count().then(function(wordCount){
                    if(wordCount > 0){
                        var randomSkip = (Math.random() * (wordCount-1))|0;
                        if(randomSkip > 0){
                            query.skip(randomSkip);
                        }
                        query.limit(1);
                        return query.find();
                    } else {
                        throw new Error("There are no words in this group!");
                    }
                });
            },
            /**
             * Gets a word, given its id.
             * @param {Object} id - objectId of the word to retrieve.
             * @return {Promise} resolved with word associated to the given id, rejected on any error.
             */
            getById : function(id){
                var query = new Parse.Query(parseObject);
                return query.get(id);
            }
        };
    }])
    .service('WordGroupService', ['$q', 'UserService', function($q, UserService){
        // Word Groups: add, edit, delete, getAll, getGroupById
        var parseObject = Parse.Object.extend("WordGroup");
        return {
            object: parseObject,
            /**
             * Adds a wordgroup.
             * @param {String} name - name of the wordgroup
             * @param {Object} description - wordgroup description
             * @param {Object} user - user or pointer to user to associate wordgroup to. (optional)
             *                        by default wordgroup is associated to current user.
             * @return {Promise} resolved when the object is saved, rejected on any error.
             */
            add : function(name, description, user){
                var promise;
                if(user){
                    promise = $q.when(user);
                } else {
                    promise = UserService.currentUser();
                }
                return promise.then(function(user){
                    var wordgroupObject = new parseObject();
                    wordgroupObject.set('name', name);
                    wordgroupObject.set('description', description);
                    wordgroupObject.set('user', user);
                    return wordgroupObject.save();
                });
                // mood, Mood
            },
            /**
             * Edits a wordgroup.
             * @param {String} wordgroupObject - object representing the wordgroup.
             * @param {Object} name - new name
             * @param {Object} description - new description
             * @return {Promise} resolved when the object is edited, rejected on any error.
             */
            edit : function(wordgroupObject, name, description){
                wordgroupObject.set("name", name);
                wordgroupObject.set("description", description);
                return wordgroupObject.save();
            },
            /**
             * Deletes a wordgroup.
             * @param {String} wordgroupObject - object representing the wordgroup.
             * @return {Promise} resolved when the object is deleted, rejected on any error.
             */
            delete : function(wordgroupObject){
            	console.log(wordgroupObject);
                return wordgroupObject.destroy();
            },
            /**
             * Gets all wordgroup associated to a given user.
             * @param {Object} user - user for which to query the wordgroups. (optional)
             *                        default is the current user.
             * @return {Promise} resolved with all wordgroups for the given user (the current user if no user was given), rejected on any error.
             */
            getAll : function(user){
                var query = new Parse.Query(parseObject);
                var promise;
                if(user){
                    promise = $q.when(user);
                } else {
                    promise = UserService.currentUser().then(function(user){
                        query.equalTo('user', user);
                    });
                }
                return $q.when(promise.then(function(){
                    return query.find();
                }));
            },
            /**
             * Gets a wordgroup, given its id.
             * @param {Object} id - objectId of the wordgroup to retrieve.
             * @return {Promise} resolved with wordgroup associated to the given id, rejected on any error.
             */
            getById : function(id){
                var query = new Parse.Query(parseObject);
                return query.get(id);
            }
        };
    }])
    .service('SavedWordListService', ['$q', 'UserService', function($q, UserService){
        // SavedWordLists: save (add), delete, getAll, getById
        var parseObject = Parse.Object.extend("SavedWordList");
        return {
            object: parseObject,
            /**
             * Adds a saved word list.
             * @param {String} words - words in the list
             * @param {Object} user - user or pointer to user to associate wordgroup to. (optional)
             *                        by default wordgroup is associated to current user.
             * @return {Promise} resolved when the object is saved, rejected on any error.
             */
            add : function(words, user){
                var promise;
                if(user){
                    promise = $q.when(user);
                } else {
                    promise = UserService.currentUser();
                }
                return promise.then(function(user){
                    var savedWordListObject = new parseObject();
                    savedWordListObject.set('words', words);
                    savedWordListObject.set('user', user);
                    return savedWordListObject.save();
                });
            },
            /**
             * Edits a saved word list.
             * @param {String} savedWordListObject - object representing the saved word list.
             * @param {Object} words - new words in the list
             * @return {Promise} resolved when the object is edited, rejected on any error.
             */
            edit : function(savedWordListObject, words){
                savedWordListObject.set("words", words);
                return savedWordListObject.save();
            },
            /**
             * Deletes a saved word list.
             * @param {String} savedWordListObject - object representing the saved word list.
             * @return {Promise} resolved when the object is deleted, rejected on any error.
             */
            delete : function(savedWordListObject){
                return savedWordListObject.destroy();
            },
            /**
             * Gets all saved word lists associated to a given user.
             * @param {Object} user - user for which to query the saved word lists. (optional)
             *                        default is the current user.
             * @return {Promise} resolved with all saved word lists for the given user (the current user if no user was given), rejected on any error.
             */
            getAll : function(user){
                var query = new Parse.Query(parseObject);
                var promise;
                if(user){
                    promise = $q.when(user);
                } else {
                    promise = UserService.currentUser().then(function(user){
                        query.equalTo('user', user);
                    });
                }
                return promise.then(function(){
                    return query.find();
                });
            },
            /**
             * Gets a saved word list, given its id.
             * @param {Object} id - objectId of the saved word list to retrieve.
             * @return {Promise} resolved with saved word list associated to the given id, rejected on any error.
             */
            getById : function(id){
                var query = new Parse.Query(parseObject);
                return query.get(id);
            }
        };
    }])
    .service('AppService', ['$q', 'UserService',
        function ($q, UserService) {
            return {
            };
        }]);

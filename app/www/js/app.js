// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter',
    [
        'ionic',
        'app.controllers',
        'app.services',
        'user.controllers',
        'user.services'
    ]
)
/**
 * see documentation: https://www.parse.com/apps/quickstart#parse_data/web/existing
 *
 * SET THESE VALUES IF YOU WANT TO USE PARSE, COMMENT THEM OUT TO USE THE DEFAULT
 * SERVICE
 *
 * parse constants
 */
    .constant('ParseConfiguration', {
        applicationId: "1k9X7s2M45Py0Af82yeyNnrhifpoPhhNpS5koaAt",
        javascriptKey: "q9d08DUOF40e1fsmH7zZySyyIb0HqC1F3zO0s5PT"
    })
    .constant('Parse', Parse)
/**
 *
 */
    .config(function ($stateProvider, $urlRouterProvider, Parse, ParseConfiguration) {
        Parse.initialize(ParseConfiguration.applicationId, ParseConfiguration.javascriptKey);

        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider
            // create account state
            .state('app-signup', {
                url: "/signup",
                templateUrl: "templates/user/signup.html",
                controller: "SignUpController"
            })
            // login state that is needed to log the user in after logout
            // or if there is no user object available
            .state('app-login', {
                url: "/login",
                templateUrl: "templates/user/login.html",
                controller: "LoginController"
            })

            // setup an abstract state for the tabs directive, check for a user
            // object here is the resolve, if there is no user then redirect the
            // user back to login state on the changeStateError
            .state('tab', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html",
                resolve: {
                    user: function (UserService) {
                        var value = UserService.init();
                        return value;
                    }
                }
            })

            // Each tab has its own nav history stack:
            .state('tab.dash', {
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl',
                    }
                },
            })
            .state('tab.group-list', {
                url: '/manage',
                views: {
                    'group-list': {
                        templateUrl: 'templates/group-list.html',
                        controller: 'GroupListCtrl'
                    }
                }
            })
            .state('tab.group-detail', {
                url: '/group/:groupId',
                views: {
                    'group-list': {
                        templateUrl: 'templates/group-detail.html',
                        controller: 'GroupDetailCtrl'
                    }
                }
            })
            .state('tab.word-detail', {
                url: '/word/:wordId',
                views: {
                    'group-list': {
                        templateUrl: 'templates/word-detail.html',
                        controller: 'WordDetailCtrl'
                    }
                }
            })
            .state('tab.branstorm-wizard', {
                url: '/brainstorm-wizard',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/brainstorm-wizard.html',
                        controller: 'BrainstormCtrl'
                    }
                }
            })
            .state('tab.branstorm-result', {
                url: '/brainstorm-result',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/brainstorm-result.html',
                        controller: 'BrainstormCtrl'
                    }
                }
            })

            .state('tab.collection-detail', {
                url: '/collection/:collectionId',
                views: {
                    'collection-detail': {
                        templateUrl: 'templates/collection-detail.html',
                        controller: 'CollectionDetailCtrl'
                    }
                }
            })

            .state('tab.settings', {
                url: '/settings',
                cache: false,
                views: {
                    'tab-settings': {
                        templateUrl: 'templates/tab-settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/dash');

    })
    .run(function ($ionicPlatform, $rootScope, $state) {


        $rootScope.$on('$stateChangeError',
            function (event, toState, toParams, fromState, fromParams, error) {



                console.log('$stateChangeError ' + error && (error.debug || error.message || error));

                // if the error is "noUser" the go to login state
                if (error && error.error === "noUser") {
                    event.preventDefault();

                    $state.go('app-login', {});
                }
            });

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })
